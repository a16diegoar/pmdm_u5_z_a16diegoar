package com.example.u5_z_a16diegoar;

import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private File file;
    private TextView tv;

    private String[] raws = new String[] {"adeus", "ola"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.tvDatos);
    }

    public void actualizaFile(View v) {
        String dir = "";
        String subdir = ((EditText) findViewById(R.id.txeDir)).getText().toString();
        if (subdir.length() > 0) {
            subdir += "/";
        }
        String nom = ((EditText) findViewById(R.id.txeFile)).getText().toString();

        if (((RadioButton) findViewById(R.id.rbInter)).isChecked()) {
            dir = getFilesDir().getAbsolutePath();
        } else if (((RadioButton) findViewById(R.id.rbSd)).isChecked()) {
            dir = Environment.getExternalStorageDirectory().getAbsolutePath();
        } else if (((RadioButton) findViewById(R.id.rbRaw)).isChecked()) {
            // TODO: abrir el recurso.
            return;
        }

        dir += "/";
        file = new File(dir+subdir+nom);
    }

    private void listRaw() {
        tv.setText("RAW Content:\n");
        for (String file : raws) {
            tv.append("\tFile: "+file);
        }
    }

    public void btnWrite(View v) {

    }

    public void btnRead(View v) {

    }

    public void btnDelete(View v) {

    }

    public void btnList(View v) {
        actualizaFile(null);

        if (((RadioButton) findViewById(R.id.rbRaw)).isChecked()) {
            listRaw();
        } else {
            tv.setText("");
            tv.setText(file.getAbsolutePath());
        }
    }
}
